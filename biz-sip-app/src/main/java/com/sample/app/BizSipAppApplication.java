package com.sample.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@ComponentScan(basePackages={"com.sample.app","com.bizmda.bizsip.app","cn.hutool.extra.spring"})
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class BizSipAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(BizSipAppApplication.class, args);
    }
}