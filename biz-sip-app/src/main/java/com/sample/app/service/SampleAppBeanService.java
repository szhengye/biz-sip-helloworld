package com.sample.app.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppClientFactory;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.app.api.AppBeanInterface;
import com.sample.client.sink.samplebean.api.HelloInterface;
import org.springframework.stereotype.Service;

@Service
public class SampleAppBeanService implements AppBeanInterface {
    private HelloInterface helloInterface = AppClientFactory
            .getSinkClient(HelloInterface.class,"sample-bean-sink");

    @Override
    public JSONObject process(JSONObject jsonObject) throws BizException {
        String message = (String)jsonObject.get("message");
        jsonObject.set("message","sample-app-bean-service: Hello,"+message+";"
                + this.helloInterface.hello(message));
        return jsonObject;
    }
}
