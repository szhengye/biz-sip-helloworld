package com.sample.app.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppClientFactory;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizMessageInterface;
import com.sample.client.app.api.SampleBeanServiceInterface;
import com.sample.client.sink.samplebean.api.HelloInterface;
import org.springframework.stereotype.Service;

@Service
public class SampleBeanService implements SampleBeanServiceInterface {
    private BizMessageInterface sampleSinkBeankSinkInterface = AppClientFactory
            .getSinkClient(BizMessageInterface.class,"sample-sink-bean-sink");
    private HelloInterface sampleBeanSinkInterface = AppClientFactory
            .getSinkClient(HelloInterface.class,"sample-bean-sink");
    private HelloInterface sampleRabbitmqBeanSinkInterface = AppClientFactory
            .getSinkClient(HelloInterface.class,"sample-rabbitmq-bean-sink");
    @Override
    public String callSampleSinkBeanSink(String message) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.set("message",message);
        BizMessage<JSONObject> bizMessage;
        try {
            bizMessage = sampleSinkBeankSinkInterface.call(jsonObject);
        } catch (BizException e) {
            e.printStackTrace();
            return null;
        }
        return (String)bizMessage.getData().get("message");
    }

    @Override
    public String callSampleBeanSink(String message) {
        return this.sampleBeanSinkInterface.hello(message);
    }

    @Override
    public String callAllSink(String message) {
        String result = "";
        JSONObject jsonObject = new JSONObject();
        jsonObject.set("message",message);
        BizMessage<JSONObject> bizMessage;
        try {
            bizMessage = sampleSinkBeankSinkInterface.call(jsonObject);
        } catch (BizException e) {
            e.printStackTrace();
            return null;
        }
        this.sampleRabbitmqBeanSinkInterface.hello(message);
        result = (String)bizMessage.getData().get("message") + "\n";
        result = result + this.sampleBeanSinkInterface.hello(message);
        return result;
    }
}
