package com.sample.client.app.api;

public interface SampleBeanServiceInterface {
    public String callSampleSinkBeanSink(String message);
    public String callSampleBeanSink(String message);
    public String callAllSink(String message);
}
