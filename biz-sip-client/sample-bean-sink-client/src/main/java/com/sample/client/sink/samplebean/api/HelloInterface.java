package com.sample.client.sink.samplebean.api;

public interface HelloInterface {
    public String hello(String message);
}
