package com.sample.sink.samplebean;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author 史正烨
 */
@Slf4j
@SpringBootApplication
@ComponentScan(basePackages={"cn.hutool.extra.spring", "com.sample.sink.samplebean"})
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class SampleBeanSinkApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleBeanSinkApplication.class, args);
    }
}