package com.sample.sink.samplebean.service;

import com.sample.client.sink.samplebean.api.HelloInterface;
import org.springframework.stereotype.Service;

@Service
public class SampleBeanService implements HelloInterface {
    @Override
    public String hello(String message) {
        return "sample-bean-sink: Hello," + message;
    }
}
