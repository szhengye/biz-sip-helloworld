package com.sample.sink.samplerabbitmqbean;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author 史正烨
 */
@Slf4j
@SpringBootApplication
@ComponentScan(basePackages={"cn.hutool.extra.spring",
        "com.bizmda.bizsip.sink.listener", "com.sample.sink.samplerabbitmqbean"})
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class SampleRabbitmqBeanSinkApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleRabbitmqBeanSinkApplication.class, args);
    }
}