package com.sample.sink.samplerabbitmqbean.service;

import com.sample.client.sink.samplebean.api.HelloInterface;
import org.springframework.stereotype.Service;

@Service
public class SampleRabbitmqBeanService implements HelloInterface {
    @Override
    public String hello(String message) {
        System.out.println("sample-rabbitmq-bean-sink: Hello," + message);
        return "sample-rabbitmq-bean-sink: Hello," + message;
    }
}
