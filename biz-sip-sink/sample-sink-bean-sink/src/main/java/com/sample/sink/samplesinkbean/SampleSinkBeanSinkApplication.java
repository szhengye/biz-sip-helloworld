package com.sample.sink.samplesinkbean;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author 史正烨
 */
@Slf4j
@SpringBootApplication
@ComponentScan(basePackages={"cn.hutool.extra.spring",
        "com.bizmda.bizsip.sink.controller", "com.sample.sink.samplesinkbean"})
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class SampleSinkBeanSinkApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleSinkBeanSinkApplication.class, args);
    }
}