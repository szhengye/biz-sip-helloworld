package com.sample.sink.samplesinkbean.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.sink.api.SinkBeanInterface;
import org.springframework.stereotype.Service;

@Service
public class SampleSinkBeanService implements SinkBeanInterface {
    @Override
    public JSONObject process(JSONObject packMessage) throws BizException {
        String message = (String)packMessage.get("message");
        packMessage.set("message","sample-sink-bean-sink: Hello,"+message);
        return packMessage;
    }
}
