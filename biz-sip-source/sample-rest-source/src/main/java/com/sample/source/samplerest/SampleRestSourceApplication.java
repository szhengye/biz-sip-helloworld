package com.sample.source.samplerest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;

/**
 * @author 史正烨
 */
@Slf4j
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class SampleRestSourceApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleRestSourceApplication.class, args);
    }
}
