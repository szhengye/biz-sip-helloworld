package com.sample.source.samplerest.controller;

import com.bizmda.bizsip.source.api.SourceClientFactory;
import com.sample.client.app.api.SampleBeanServiceInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 史正烨
 */
@Slf4j
@RestController
@RequestMapping("/rest")
public class SampleRestController {
    private SampleBeanServiceInterface sampleBeanServiceInterface = SourceClientFactory
            .getAppServiceClient(SampleBeanServiceInterface.class,"/app/sample-bean-service");

    @GetMapping(value ="/callSampleBeanSink")
    public String callSampleBeanSink(String message) {
        return this.sampleBeanServiceInterface.callSampleBeanSink(message);
    }

    @GetMapping(value ="/callSampleSinkBeanSink")
    public String callSampleSinkBeanSink(String message) {
        return this.sampleBeanServiceInterface.callSampleSinkBeanSink(message);
    }

    @GetMapping(value ="/callAllSink")
    public String callAllSink(String message) {
        return this.sampleBeanServiceInterface.callAllSink(message);
    }
}