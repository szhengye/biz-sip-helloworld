#!/bin/sh
set -v on
curl http://localhost:9001/rest/callSampleBeanSink\?message=world
curl http://localhost:9001/rest/callSampleSinkBeanSink\?message=world
curl http://localhost:9001/rest/callAllSink\?message=world
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/sample-sink-service" -X POST --data '{"message":"world"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/app/sample-bean-service" -X POST --data '{"methodName":"callSampleSinkBeanSink","params":["world"]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/app/sample-bean-service" -X POST --data '{"methodName":"callSampleBeanSink","params":["world"]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/app/sample-bean-service" -X POST --data '{"methodName":"callAllSink","params":["world"]}' http://localhost:8888/api|jq